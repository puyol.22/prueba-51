@extends('layouts/app')
@section('content')
 <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Clients</h1>
          <p class="mb-4 ">Add  <a  href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></a></p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">List Clients</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>rut</th>
                      <th>business</th>
                      <th>phone </th>
                      <th>mail</th>
                      <th class="">Actions</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach($client as $cl)
                    <tr>
                      <td>{{$cl->name}}</td>
                      <td>{{$cl->rut}}</td>
                      <td>{{$cl->socialName}}</td>
                      <td>{{$cl->phone}}</td>
                      <td>{{$cl->email}}</td>
                      <td class="align-middle">
                      	<button class="btn btn-primary" name="edit" id="{{$cl->id}}">
                      		<i class="fa fa-pencil fa-1x" aria-hidden="true"></i>
                      	</button>
                      	<button  class="btn btn-danger" name="remove" id="{{$cl->id}}">
						              <i class="fa fa-trash-o fa-lg"></i>
						            </button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>


        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <form action="{{route('Clients.store')}}" method="post">
      {!! csrf_field() !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Client</h4>
      </div>
      <div class="modal-body">

		    <div class="form-group">
		      <label for="name">name:</label>
		      <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" required="required">
		    </div>
		    <div class="form-group">
		      <label for="pwd">business name:</label>
		      <input type="text" class="form-control" id="socialName" placeholder="Enter business name" name="socialName" required="required">
		    </div>

		    <div class="form-group">
		      <label for="pwd">rut:</label>
		      <input type="text" class="form-control" id="rut" placeholder="Enter rut" name="rut" required="required">
		    </div>

		    <div class="form-group">
		      <label for="pwd">address:</label>
		      <input type="text" class="form-control" id="address" placeholder="Enter address" name="address" required="required">
		    </div>

         <div class="form-group">
          <label for="pwd">phone:</label>
          <input type="text" class="form-control" id="phon" placeholder="Enter phone" name="phone" required="required">
        </div>

			<div class="form-group">
		      <label for="pwd">mail:</label>
		      <input type="text" class="form-control" id="mail" placeholder="Enter mail" name="mail" required="required">
		    </div>

      </div>

      <div class="modal-footer">
      	<button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

      </div>

		  </form>
    </div>

  </div>
</div>


<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      {!! csrf_field() !!}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Client</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idEdit" id="idEdit" value="">

        <div class="form-group">
          <label for="name">name:</label>
          <input type="text" class="form-control" id="nameEdit" placeholder="Enter Name" name="nameEdit" required="required">
        </div>
        <div class="form-group">
          <label for="pwd">business name:</label>
          <input type="text" class="form-control" id="socialNameEdit" placeholder="Enter business name" name="socialNameEdit" required="required">
        </div>

        <div class="form-group">
          <label for="pwd">rut:</label>
          <input type="text" class="form-control" id="rutEdit" placeholder="Enter rut" name="rutEdit" required="required">
        </div>

        <div class="form-group">
          <label for="pwd">address:</label>
          <input type="text" class="form-control" id="addressEdit" placeholder="Enter address" name="addressEdit" required="required">
        </div>

         <div class="form-group">
          <label for="pwd">phone:</label>
          <input type="text" class="form-control" id="phoneEdit" placeholder="Enter phone" name="phoneEdit" required="required">
        </div>

      <div class="form-group">
          <label for="pwd">mail:</label>
          <input type="text" class="form-control" id="mailEdit" placeholder="Enter mail" name="mailEdit" required="required">
        </div>

      </div>

      <div class="modal-footer">
        <button type="buttom" class="btn btn-success" id="editBtn">Edit</button>
        <button type="button" class="btn btn-danger" id="cancelBtn" data-dismiss="modal">Close</button>
      </div>

    </div>

  </div>
</div>


        <!-- /.container-fluid -->
@endsection

@section('jsCustom')

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script type="text/javascript">
	$( document ).ready(function() {
     const fileHeaders = {
                'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                'content-type': 'multipart/form-data',
                'Accept': 'application/json',
                }

          $("button[name = 'edit']").click(function(){
              $('#idEdit').val(this.id)
                $.ajax({
                  type: "Get",
                  url: 'Clients/'+this.id,
                  dataType: "json",
                  headers:fileHeaders
              }).done(response=>{
                $('#nameEdit').val(response.name);
                $('#socialNameEdit').val(response.socialName);
                $('#rutEdit').val(response.rut);
                $('#addressEdit').val(response.address);
                $('#phoneEdit').val(response.phone);
                $('#mailEdit').val(response.email);
                $('#editModal').modal('show');
              });

    })


    $('#editBtn').click(function(){
        var obj = {
          name:$('#nameEdit').val(),
          socialName:$('#socialNameEdit').val(),
          rut:$('#rutEdit').val(),
          address:$('#addressEdit').val(),
          phone:$('#phoneEdit').val(),
          mail:$('#mailEdit').val()
        }

                  $.ajax({
                    type: "patch",
                    url: 'Clients/'+$('#idEdit').val(),
                    data:JSON.stringify(obj),
                    dataType: "json",
                    headers:fileHeaders
                  }).done(response=>{
                     location.reload(true)
                  }).fail(function() {

                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Something went wrong!',
                      footer: '<a href>Why do I have this issue?</a>'
                    })
                  })


    })

    $("button[name = 'remove']").click(function(){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
           $.ajax({
                    type: "delete",
                    url: 'Clients/'+this.id,
                    dataType: "json",
                    headers:fileHeaders
                  }).done(response=>{
                     location.reload(true)
                  })
        }
      })
    })
});
</script>
@endsection