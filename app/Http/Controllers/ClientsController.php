<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class ClientsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct() {
		$this->middleware('auth');
	}
	public function index() {
		//listar clientes
		$client = new Client();
		return View('app/clients/index', ['client' => $client::all()]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		dd($request->all());
		//
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'rut' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('app/clients/index')
				->withErrors($validator)
				->withInput();
		} else {
			$client = new Client();
			$client->name = $request->name;
			$client->phone = $request->phone;
			$client->rut = $request->rut;
			$client->address = $request->address;
			$client->socialName = $request->socialName;
			$client->email = $request->mail;
			$client->save();
			return View('app/clients/index', ['client' => $client::all()]);
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$clien = new Client();
		$rs = $clien::find($id);
		return $rs->toJson();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
		$data = json_decode($request->getContent());
		$client = new Client();
		$cl = $client::find($id);
		$cl->name = $data->name;
		$cl->phone = $data->phone;
		$cl->rut = $data->rut;
		$cl->address = $data->address;
		$cl->socialName = $data->socialName;
		$cl->email = $data->mail;
		return $cl->save() ?
		$cl->toJson() : // returns 200 OK status with contact (json)
		response($cl->errors, 400);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
		$client = new Client();
		$cl = $client::find($id);
		return $cl->delete() ?
		$cl->toJson() : // returns 200 OK status with contact (json)
		response($cl->errors, 400);

	}
}
